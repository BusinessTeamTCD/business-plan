package findyourguide.com.findyourguideapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by yamin on 15/3/10.
 */
public class ChatActivity extends Activity{
    private static String GETMESSAGE_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/androidgetMessage?userID2=";
    private static String SENDMESSAGE_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/androidsendMessage?userID1=";

    private TextView textView;
    private EditText editText;
    private ImageButton imageButton;
    private SendMessageTask sendMessageTask;
    private GetMessageTask getMessageTask;
    private String guideID;
    private int guideid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);
        guideID = getIntent().getStringExtra("guide_ID");
        guideid = Integer.parseInt(guideID);
        getMessageTask = new GetMessageTask();
        getMessageTask.execute(GETMESSAGE_URL + MainActivity.USER_ID + "&guide_ID1=" + guideid);


        editText = (EditText) findViewById(R.id.sendMessageText);
        imageButton = (ImageButton) findViewById(R.id.sendMessageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessageTask = new SendMessageTask();
                sendMessageTask.execute(SENDMESSAGE_URL + MainActivity.USER_ID + "&guide_ID=" + guideid + "&content=" + editText.getText().toString());
            }
        });
    }


    private class GetMessageTask extends AsyncTask {
        private JSONArray msgls;
        private JSONObject jsonObject;
        private String msg = "";

        @Override
        protected void onPreExecute() {
            textView = (TextView) findViewById(R.id.chatmessageText);
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            textView.setText(msg);
        }

        @Override
        protected Object doInBackground(Object[] params) {

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet request = new HttpGet((String) params[0]);

                HttpResponse response = httpClient.execute(request);

                if (response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();
                    String json = EntityUtils.toString(entity, "UTF-8");
                    jsonObject = new JSONObject(json);
                    msgls = jsonObject.getJSONArray("msgls");
                    for (int i = 0; i < msgls.length(); i++) {
                        msg = msg + msgls.get(i) + "\n"+"\n";
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private class SendMessageTask extends AsyncTask {
        private boolean sendmsg;
        private JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            if (sendmsg == true) {
                editText.setText("");
                AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                builder.setTitle("Remind").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setMessage("Message sending succeeds!").create().show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                builder.setTitle("Remind").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setMessage("Message sending failed!").create().show();
            }
        }

        @Override
        protected Object doInBackground(Object[] params) {

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet request = new HttpGet((String) params[0]);

                HttpResponse response = httpClient.execute(request);

                if (response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();
                    String json = EntityUtils.toString(entity, "UTF-8");
                    jsonObject = new JSONObject(json);
                    sendmsg = (boolean) jsonObject.get("sendmsg");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

}
