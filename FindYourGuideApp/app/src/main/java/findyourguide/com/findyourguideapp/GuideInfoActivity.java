package findyourguide.com.findyourguideapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by yamin on 15/3/10.
 */
public class GuideInfoActivity extends Activity{
    private static String ACTION_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/androidGuideDetails?guide_ID=";

    private Gallery gallery;
    private TextView name;
    private TextView city;
    private TextView word;
    private TextView country;
    private ImageView imageMan;
    private MyTask myTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.guidedetail);
        myTask=new MyTask();
        myTask.execute(ACTION_URL+getIntent().getStringExtra("guide_ID"));

       // Log.i("yes",getIntent().getStringExtra("guide_ID"));
    }
    private class photosAdapter extends BaseAdapter{
        private Context context;
        private List<Drawable> photos;

        public photosAdapter(List<Drawable> photos,Context context){
            this.context=context;
            this.photos=photos;
        }
        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView=new ImageView(this.context);
            imageView.setBackground(photos.get(position));
            imageView.setLayoutParams(new Gallery.LayoutParams(900,1500));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            return imageView;
        }
    }
    private class MyTask extends AsyncTask{
        private List<Drawable> photos;
        private boolean status=false;
        private Map<String,Object> guideMap;
        @Override
        protected void onPreExecute() {
            gallery=(Gallery)findViewById(R.id.gallery);
            name=(TextView)findViewById(R.id.guidename);
            city=(TextView)findViewById(R.id.cityText);
            country=(TextView)findViewById(R.id.countryText);
            word=(TextView)findViewById(R.id.guideText);
            imageMan=(ImageView)findViewById(R.id.imageMan);
            guideMap=new HashMap<String,Object>();
            photos=new ArrayList<Drawable>();
            Toast.makeText(GuideInfoActivity.this, "loading...", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try{
                HttpClient httpClient=new DefaultHttpClient();
                HttpGet request=new HttpGet((String)params[0]);
               // Log.i("URL",(String)params[0]);
                HttpResponse response=httpClient.execute(request);
                if(response.getStatusLine().getStatusCode()==200){
                    HttpEntity entity=response.getEntity();
                    String json= EntityUtils.toString(entity,"UTF-8");
                    JSONObject jsonObject=new JSONObject(json);
                    JSONArray jsonArray=jsonObject.getJSONArray("photosList");
                    for(int i=0;i<jsonArray.length();i++){
                        //Log.i("URL",(String)jsonArray.get(i));
                        photos.add(loadImageFromNetwork((String)jsonArray.get(i)));
                    }

                    JSONObject guide=jsonObject.getJSONObject("guide");
                    guideMap.put("name",guide.get("name"));
                    guideMap.put("city",guide.get("city"));
                    guideMap.put("country",guide.get("nation"));
                    guideMap.put("word",guide.get("tips"));
                    guideMap.put("imageMan",loadImageFromNetwork((String)guide.get("headicon")));
                    status=true;
                }

            }catch(IOException e){
                e.printStackTrace();
            }catch(JSONException e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            if(status==false){
                AlertDialog.Builder builder=new AlertDialog.Builder(GuideInfoActivity.this);
                builder.setTitle("Remind").setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();


                    }
                }).setMessage("Load Fail!:(").create().show();
            }else {
                gallery.setAdapter(new photosAdapter(photos,GuideInfoActivity.this));
                name.setText((String)guideMap.get("name"));
                city.setText((String)guideMap.get("city"));
                country.setText((String)guideMap.get("country"));
                word.setText((String)guideMap.get("word"));
                imageMan.setBackground((Drawable)guideMap.get("imageMan"));
            }
        }
    }
    private Drawable loadImageFromNetwork(String url){
        Drawable drawable = null;
        try{
            //judge if has picture locate or not according to filename
            drawable = Drawable.createFromStream(new URL(url).openStream(), "photo.jpg");
        }catch(IOException e){
            Log.d("test",e.getMessage());
        }
        if(drawable == null){
            Log.d("test","null drawable");
        }else{
            Log.d("test","not null drawable");
        }
        return drawable;

    }
}
