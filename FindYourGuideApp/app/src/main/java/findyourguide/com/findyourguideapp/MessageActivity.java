package findyourguide.com.findyourguideapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by yamin on 15/3/10.
 */
public class MessageActivity extends Activity{

    private static String SHOWMESSAGE_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/androidshowMessage?userID=";

    private TextView textView;
    private MyTask myTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message);
        myTask = new MyTask();
//        while(true){
            myTask.execute(SHOWMESSAGE_URL+MainActivity.USER_ID);
//            try {
//                myTask.wait(3000);
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
    }

    private class MyTask extends AsyncTask {
        private JSONArray msgls;
        private boolean login;
        private JSONObject jsonObject;
        private String msg="";

        @Override
        protected void onPreExecute() {
            textView = (TextView)findViewById(R.id.showmessageText);
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            if (login == true) {
                textView.setText(msg);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MessageActivity.this);
                builder.setTitle("Remind").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setMessage("Please login").create().show();
            }
        }

        @Override
        protected Object doInBackground(Object[] params) {

            try {
                HttpClient httpClient=new DefaultHttpClient();
                HttpGet request=new HttpGet((String)params[0]);
                //Log.i("url",(String)params[0]);

                HttpResponse response=httpClient.execute(request);

                if(response.getStatusLine().getStatusCode()==200){
                    HttpEntity entity=response.getEntity();
                    String json= EntityUtils.toString(entity,"UTF-8");
                    jsonObject=new JSONObject(json);
                    login = (boolean) jsonObject.get("login");
                    if(login==true){
                        msgls = jsonObject.getJSONArray("msgls");
                        for(int i=0; i<msgls.length(); i++){
                            msg=msg+msgls.get(i)+"\n"+"\n";
                        }
                    }
                    }
                }catch (JSONException e) {
                e.printStackTrace();
                }catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }



    }


}
