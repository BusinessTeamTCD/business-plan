package findyourguide.com.findyourguideapp;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yamin on 15/3/10.
 */
public class OrderActivity extends ListActivity {
    private static String ORDER_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/androidOrderDetails.action?User_ID=";
    private List<Map<String,Object>> orderList;
    private boolean status=false;

    private MyTask myTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderList=new ArrayList<Map<String,Object>>();
        //setContentView(R.layout.order);
        myTask=new MyTask();
        myTask.execute(ORDER_URL);



    }

    private List<Map<String, Object>> getData() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        Map<String, Object> map = new HashMap<String, Object>();


        map = new HashMap<String, Object>();
        map.put("guideName", "Yunyi Zhu");
        map.put("startDate", "09/03/2015");
        map.put("endDate", "09/04/2015");
        map.put("city","Galway");
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("guideName", "Huahua Li");
        map.put("startDate", "09/03/2015");
        map.put("endDate", "09/04/2015");
        map.put("city","Cork");
        list.add(map);

        return list;

    }
    private class MyTask extends AsyncTask{
        private List<Map<String,Object>> list;
        private boolean success=false;

        @Override
        protected Object doInBackground(Object[] params) {
            try{
                HttpClient httpClient=new DefaultHttpClient();
                if(MainActivity.USER_ID==0){

                    return null;
                }
                HttpGet request=new HttpGet(ORDER_URL+String.valueOf(MainActivity.USER_ID));
                HttpResponse response=httpClient.execute(request);
                if(response.getStatusLine().getStatusCode()==200){
                    HttpEntity entity=response.getEntity();
                    String json= EntityUtils.toString(entity,"UTF-8");
                    JSONObject jsonObject=new JSONObject(json);
                    JSONArray jsonArray=jsonObject.getJSONArray("orderlist");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject obj=jsonArray.getJSONObject(i);
                        Map<String,Object> map =new HashMap<String,Object>();
                        map.put("guideName",String.valueOf((int) obj.get("guide_ID")) );
                        map.put("startDate", (String)obj.get("startTime"));
                        map.put("endDate", (String)obj.get("endTime"));
                        map.put("city",(String) obj.get("city"));
                        list.add(map);
                    }

                }
            }catch(IOException e){
                e.printStackTrace();
            }catch(JSONException e){
                e.printStackTrace();
            }
            success=true;
            return null;
        }

        @Override
        protected void onPreExecute() {
            orderList=new ArrayList<Map<String,Object>>();
            list=new ArrayList<Map<String,Object>>();

        }

        @Override
        protected void onPostExecute(Object o) {
            if(success==false){
                AlertDialog.Builder builder=new AlertDialog.Builder(OrderActivity.this);
                builder.setTitle("Remind").setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(status==true) {
                            Intent intent = new Intent(OrderActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    }
                }).setMessage("Please Login!").create().show();
            }else {
                orderList = list;


                SimpleAdapter adapter = new SimpleAdapter(OrderActivity.this, orderList, R.layout.order,
                        new String[]{"guideName", "startDate", "endDate", "city"},
                        new int[]{R.id.guideNameText, R.id.startDateText, R.id.endDateText, R.id.ordercityText});
                setListAdapter(adapter);
            }
        }
    }
}
