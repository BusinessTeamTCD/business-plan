package findyourguide.com.findyourguideapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOError;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by jocar_000 on 2/25/2015.
 */
public class LoginActivity extends Activity {


    private EditText email;
    private EditText pwd;
    private Button login;
    private Button SignUp;

    private static final String LOGIN_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/androidLogin.action?";
    public static final String GPS_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/GPSupdate";
    private static boolean status=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                .detectDiskReads()
//                .detectDiskWrites()
//                .detectNetwork()   // or .detectAll() for all detectable problems
//                .penaltyLog()
//                .build());
//        //设置虚拟机的策略
//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                .detectLeakedSqlLiteObjects()
//                        //.detectLeakedClosableObjects()
//                .penaltyLog()
//                .penaltyDeath()
//                .build());
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView(){
        email=(EditText)findViewById(R.id.email);
        pwd=(EditText)findViewById(R.id.password);
        login=(Button)findViewById(R.id.email_sign_in_button);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String changedURL=LOGIN_URL+"email="+email.getText().toString()+"&pwd="+pwd.getText().toString();
                MyTask task=new MyTask();
                task.execute(changedURL);

            }
        });
        SignUp=(Button)findViewById(R.id.email_sign_up_button);
        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });
    }

    private void login(String email,String pwd){
        String message="";
        HttpClient httpClient=new DefaultHttpClient();
        String changedURL=this.LOGIN_URL+"email="+email+"&pwd="+pwd;
        HttpGet request=new HttpGet(changedURL);
        request.addHeader("Accept","text/json");
        Log.i("url",changedURL);
        try {
            HttpResponse response = httpClient.execute(request);
            HttpEntity httpEntity=response.getEntity();
            String json= EntityUtils.toString(httpEntity,"UTF-8");
            if(json!=null){
                JSONObject jsonObject=new JSONObject(json);
                boolean result=(boolean)jsonObject.get("success");
                status=result;
                if(result==true){
                    message="Login Success!";

                    Context context=LoginActivity.this;
                    SharedPreferences sharedPreferences=context.getSharedPreferences("SP",MODE_PRIVATE);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    JSONObject user=jsonObject.getJSONObject("user");

                    editor.putString("email",(String)user.get("email"));
                    editor.putString("first_name",(String)user.get("first_name"));
                    editor.putString("last_name",(String)user.get("last_name"));

                    editor.commit();

                    MainActivity.USER_ID=(int)user.get("User_ID");

                }else{
                    message="Login Fail!";
                }

            }
        }
        catch(IOException e){
           e.printStackTrace();
            message="Login Fail!";
        }
        catch (JSONException e){
            e.printStackTrace();
            message="Login Fail!";
        }
        AlertDialog.Builder builder=new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Remind").setPositiveButton("OK",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if(status==true) {
                    Intent intent = new Intent(LoginActivity.this, OrderActivity.class);
                    startActivity(intent);
                }
            }
        }).setMessage(message).create().show();

    }
    private class MyTask extends AsyncTask{
        private String message="";
        @Override
        protected Object doInBackground(Object[] params) {

            HttpClient httpClient=new DefaultHttpClient();

            HttpGet request=new HttpGet((String)params[0]);


            try {
                HttpResponse response = httpClient.execute(request);
                HttpEntity httpEntity=response.getEntity();
                String json= EntityUtils.toString(httpEntity,"UTF-8");
                if(json!=null){
                    JSONObject jsonObject=new JSONObject(json);
                    boolean result=(boolean)jsonObject.get("success");
                    status=result;
                    if(result==true){
                        message="Login Success!";


                        Context context=LoginActivity.this;
                        SharedPreferences sharedPreferences=context.getSharedPreferences("SP",MODE_PRIVATE);
                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        JSONObject user=jsonObject.getJSONObject("user");

                        editor.putString("email",(String)user.get("email"));
                        editor.putString("first_name",(String)user.get("first_name"));
                        editor.putString("last_name",(String)user.get("last_name"));

                        editor.commit();

                        MainActivity.USER_ID=(int)user.get("ID");



                    }else{
                        message="Login Fail!";
                    }

                }
            }
            catch(IOException e){
                e.printStackTrace();
                message="Login Fail!";
            }
            catch (JSONException e){
                e.printStackTrace();
                message="Login Fail!";
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Object o) {
            AlertDialog.Builder builder=new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle("Remind").setPositiveButton("OK",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if(status==true) {
                        GPS_Task gps_task=new GPS_Task();
                        gps_task.execute(GPS_URL);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
            }).setMessage(message).create().show();
        }
    }
    private class GPS_Task extends AsyncTask{
        private GPSTracker tracker;
        @Override
        protected void onPreExecute() {
            tracker=new GPSTracker(LoginActivity.this);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                if (tracker.canGetLocation()) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost request = new HttpPost((String) params[0]);
                    List<NameValuePair> paramList = new ArrayList<NameValuePair>();
                    BasicNameValuePair param = new BasicNameValuePair("user_ID", String.valueOf(MainActivity.USER_ID));
                    paramList.add(param);
                    param = new BasicNameValuePair("latitude", String.valueOf(tracker.getLatitude()));
                    paramList.add(param);
                    param = new BasicNameValuePair("longtitude", String.valueOf(tracker.getLongtitude()));
                    paramList.add(param);


                    request.setEntity(new UrlEncodedFormEntity(paramList, HTTP.UTF_8));
                    client.execute(request);
                }
            }catch(UnsupportedEncodingException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }
}
