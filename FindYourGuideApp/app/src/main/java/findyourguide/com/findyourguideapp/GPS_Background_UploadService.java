package findyourguide.com.findyourguideapp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jocar_000 on 3/20/2015.
 */
public class GPS_Background_UploadService extends Service {
    private static final String ACTION_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/GPSupdate";
    private MyTask myTask;
    private Context context;
    public GPS_Background_UploadService(Context context){
        this.context=context;
    }
    public void startService(){
        if(MainActivity.USER_ID!=0){
            myTask =new MyTask();
            myTask.execute(ACTION_URL);
        }
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private class MyTask extends AsyncTask{
        private GPSTracker tracker;
        @Override
        protected void onPreExecute() {
            tracker=new GPSTracker(getApplicationContext());
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                if (tracker.canGetLocation()) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost request = new HttpPost((String) params[0]);
                    List<NameValuePair> paramList = new ArrayList<NameValuePair>();
                    BasicNameValuePair param = new BasicNameValuePair("user_ID", String.valueOf(MainActivity.USER_ID));
                    paramList.add(param);
                    param = new BasicNameValuePair("latitude", String.valueOf(tracker.getLatitude()));
                    paramList.add(param);
                    param = new BasicNameValuePair("longtitude", String.valueOf(tracker.getLongtitude()));
                    paramList.add(param);


                    request.setEntity(new UrlEncodedFormEntity(paramList, HTTP.UTF_8));
                    client.execute(request);
                }
            }catch(UnsupportedEncodingException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }
}
