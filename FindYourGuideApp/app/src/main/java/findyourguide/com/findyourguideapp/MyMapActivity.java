package findyourguide.com.findyourguideapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jocar_000 on 3/19/2015.
 */
public class MyMapActivity extends Activity {
    private static final String ACTION_URL="http://"+MainActivity.IPADDRESS+":8080/GuideFinder/GPSGet?user_ID=";
    private GoogleMap map;
    private ImageButton refresh;
    private ImageButton findguide;
    private GPS_Task gps_task;

    private Marker selfMarker;
    private Marker guideMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initView();
        if(MainActivity.USER_ID!=0) {
            gps_task = new GPS_Task();
            gps_task.execute(LoginActivity.GPS_URL);
        }
        freshSelfLocation();

    }
    private void initView(){

        map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map_fragment)).getMap();
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        refresh=(ImageButton)findViewById(R.id.map_refresh_button);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                freshSelfLocation();
            }
        });
        findguide=(ImageButton)findViewById(R.id.map_guide_button);
        findguide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetRPSfromServer getRPSfromServer=new GetRPSfromServer();
                getRPSfromServer.execute(ACTION_URL);
            }
        });
    }
    private void freshSelfLocation(){
        GPSTracker tracker=new GPSTracker(MyMapActivity.this);

        if(tracker.canGetLocation()){
            if(MainActivity.USER_ID!=0) {
                gps_task=new GPS_Task();
                gps_task.execute(LoginActivity.GPS_URL);
            }
            LatLng coordinate=new LatLng(tracker.getLatitude(),tracker.getLongtitude());
            if(selfMarker!=null){
                selfMarker.remove();
            }
           selfMarker= map.addMarker(new MarkerOptions().position(coordinate).title("I AM HERE").
                   icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            CameraUpdate update= CameraUpdateFactory.newLatLngZoom(coordinate,16);
            map.animateCamera(update);
        }
    }
    private class GPS_Task extends AsyncTask {
        private GPSTracker tracker;
        @Override
        protected void onPreExecute() {
            tracker=new GPSTracker(MyMapActivity.this);
        }

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                if (tracker.canGetLocation()) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost request = new HttpPost((String) params[0]);
                    List<NameValuePair> paramList = new ArrayList<NameValuePair>();
                    BasicNameValuePair param = new BasicNameValuePair("user_ID", String.valueOf(MainActivity.USER_ID));
                    paramList.add(param);
                    param = new BasicNameValuePair("latitude", String.valueOf(tracker.getLatitude()));
                    paramList.add(param);
                    param = new BasicNameValuePair("longtitude", String.valueOf(tracker.getLongtitude()));
                    paramList.add(param);


                    request.setEntity(new UrlEncodedFormEntity(paramList, HTTP.UTF_8));
                    client.execute(request);
                }
            }catch(UnsupportedEncodingException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }
    private class GetRPSfromServer extends AsyncTask{
        String msg="";
        LatLng guideCoordinate;
        String date="";

        @Override
        protected Object doInBackground(Object[] params) {
            if(MainActivity.USER_ID!=0){
                try{
                    HttpClient client=new DefaultHttpClient();
                    HttpGet request=new HttpGet((String)params[0]+String.valueOf(MainActivity.USER_ID));
                    HttpResponse response=client.execute(request);
                    if(response.getStatusLine().getStatusCode()==200){
                        HttpEntity entity=response.getEntity();
                        String json= EntityUtils.toString(entity,"UTF-8");
                        JSONObject jsonObject=new JSONObject(json);
                        if((boolean)jsonObject.get("success")==true){
                            JSONObject guide_GPS=jsonObject.getJSONObject("guideGPS");
                            guideCoordinate=new LatLng(guide_GPS.getDouble("latitude"),guide_GPS.getDouble("longtitude"));
                            date=String.valueOf(guide_GPS.get("datetime"));
                        }
                        else{
                            msg="No Available Guide!";
                        }
                    }

                }catch(IOException e){
                    e.printStackTrace();
                    msg="Service Fail!";
                }catch(JSONException e){
                    msg="Service Fail!";
                    e.printStackTrace();
                }
            }else{
                msg="Please Login!";
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Object o) {
            if(msg.equals("No Available Guide!")||msg.equals("Service Fail!")||msg.equals("Please Login!")){
                AlertDialog.Builder builder=new AlertDialog.Builder(MyMapActivity.this);
                builder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setTitle("Warning").setMessage(msg).create().show();
            }else{
                if(guideMarker!=null){
                    guideMarker.remove();
                }
                guideMarker = map.addMarker(new MarkerOptions().position(guideCoordinate).title(date));
                CameraUpdate update= CameraUpdateFactory.newLatLngZoom(guideCoordinate,16);
                map.animateCamera(update);

            }
        }
    }
}
