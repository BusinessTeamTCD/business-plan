package com.example.driftbottle;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.Calendar;

public class PickupBottle extends Activity {
	private  AnimationDrawable  ad;
	private  ImageView  pick_spray1,pick_spray2,voice_msg,close;
	private  RelativeLayout pick_up_layout;
	int hour,minute,sec;

    private ImageButton imgbtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pick_up_bottle);

        imgbtn = (ImageButton)findViewById(R.id.bottle_picked_voice_msg);
        imgbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PickupBottle.this, ShowMessage.class);
                startActivity(intent);
            }
        });

		pick_spray1=(ImageView) findViewById(R.id.pick_spray1);
		pick_spray2=(ImageView) findViewById(R.id.pick_spray2);
		voice_msg=(ImageView) findViewById(R.id.bottle_picked_voice_msg);
		close=(ImageView) findViewById(R.id.bottle_picked_close);
		pick_up_layout=(RelativeLayout) findViewById(R.id.pick_up_layout);
		

				Calendar c=Calendar.getInstance();
				c.setTimeInMillis(System.currentTimeMillis());
				hour=c.get(Calendar.HOUR_OF_DAY);
				minute=c.get(Calendar.MINUTE);
				sec=c.get(Calendar.SECOND);
				if(hour>=18 || hour<=6){
					pick_up_layout.setBackgroundResource(R.drawable.bottle_night_bg);
				}
				else{
					pick_up_layout.setBackgroundResource(R.drawable.bottle_day_bg);
				}
		close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				pick_spray1.setVisibility(View.VISIBLE);
				ad.setOneShot(true);
				ad.start();
			}
		}, 1000);
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				pick_spray1.setVisibility(View.GONE);
				pick_spray2.setVisibility(View.VISIBLE);
				ad.setOneShot(true);
				ad.start();
			}
		}, 2000);
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				pick_spray1.setVisibility(View.GONE);
				pick_spray2.setVisibility(View.GONE);
				voice_msg.setVisibility(View.VISIBLE);
				doStartAnimation(R.anim.pick_up_scale);
				close.setVisibility(View.VISIBLE);
			}
		}, 3000);
		
		
	}
	
	private void doStartAnimation(int animId){
    	Animation animation=AnimationUtils.loadAnimation(this,animId);
    	voice_msg.startAnimation(animation);
    }
	

			@Override
	public void onWindowFocusChanged(boolean hasFocus) {
				// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		ad=(AnimationDrawable) getResources().getDrawable(R.anim.pick_up_spray);
		if(pick_spray1!=null&&pick_spray2!=null){
			pick_spray1.setBackgroundDrawable(ad);
			pick_spray2.setBackgroundDrawable(ad);
		}
				
	}
	

}
